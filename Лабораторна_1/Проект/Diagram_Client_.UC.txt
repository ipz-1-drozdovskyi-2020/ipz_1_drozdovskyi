@startuml
left to right direction
actor customer
actor Admin 

  customer --> (Log in)
  (Log in)<..(Sign up): extends
  (Log in) --> Server
   
   
   customer --> (Find ticket)
   (Find ticket)-->Server

   
   (Buy ticket) --> Server
   customer --> (Buy ticket)


   customer --> (View cart)
   (View cart)-->Server

   customer-->(Delete ticket from cart)
   (Delete ticket from cart)-->Server

actor Admin
Admin-->(Add new tickets) 
(Add new tickets)-->Server

Admin-->(Delete tickets) 
(Delete tickets)-->Server
@enduml