@startuml
start
 : Get ticket id from client<
 :Process request 0,5s;
 :Search for ticket in db]
if( Ticket  exist in database) then (Yes)
  :Add ticket to user cart in db]
  :Send ticket added successfully response> 

else (No)
  :Send ticket not available response> 
endif
end
@enduml