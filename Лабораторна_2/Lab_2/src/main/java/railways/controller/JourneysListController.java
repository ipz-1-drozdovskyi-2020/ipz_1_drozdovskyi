package railways.controller;

import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.event.Event;
import javafx.fxml.FXML;
import javafx.scene.Node;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.BorderPane;
import railways.config.MainConfig;
import railways.controller_elements.AdminJourneyListCallback;
import railways.controller_elements.JourneyListCallback;
import railways.controller_elements.JourneyTableRow;
import railways.controller_elements.ViewLoader;
import railways.network.OperationType;
import railways.network.RequestCode;
import railways.network.Response;
import railways.network.client.Client;
import railways.network.model.Journey;
import railways.network.model.User;

import java.util.List;
import java.util.stream.Collectors;

public class JourneysListController {

    @FXML
    private TableView table;

    @FXML
    private Button userJourneys;

    @FXML
    private Label welcomeMessage;

    @FXML
    private Pagination pagination;

    private TableColumn from = new TableColumn("From");
    private TableColumn to = new TableColumn("To");
    private TableColumn places = new TableColumn("Places");
    private TableColumn date = new TableColumn("Date");

    @FXML
    public void initialize() {
        pagination.currentPageIndexProperty().addListener(
                (observable, oldValue, newValue) -> {
                    Response response = Client.getInstance().send(
                            OperationType.GET_JOURNEYS_FOR_PERIOD,
                            newValue.intValue()
                    );
                    table.setItems(FXCollections.observableList(((List<Journey>) response.getData())
                            .stream().map(JourneyTableRow::new).collect(Collectors.toList())
                    ));
                }
        );

        Response response = Client.getInstance().send(OperationType.GET_JOURNEYS_FOR_PERIOD, 0);
        User user = MainConfig.getUser();

        from.setCellValueFactory(new PropertyValueFactory<JourneyTableRow, String>("from"));
        to.setCellValueFactory(new PropertyValueFactory<JourneyTableRow, String>("to"));
        places.setCellValueFactory(new PropertyValueFactory<JourneyTableRow, String>("places"));
        date.setCellValueFactory(new PropertyValueFactory<JourneyTableRow, String>("date"));

        table.setItems(FXCollections.observableList(((List<Journey>) response.getData())
                .stream().map(JourneyTableRow::new).collect(Collectors.toList())
        ));

        table.setEditable(false);
        table.setColumnResizePolicy(TableView.CONSTRAINED_RESIZE_POLICY);
        table.getColumns().addAll(from, to, places, date);

        if (user.isAdmin()) {
            welcomeMessage.setText("Welcome to Admin page");
            userJourneys.setText("Add journey");
            table.setRowFactory(new AdminJourneyListCallback(table));
        } else {
            welcomeMessage.setText("Hello " + user.getName() + " " + user.getSurname());

            if (response.getRequestCode().equals(RequestCode.CONNECTION_ERROR)) {
                return;
            }

            table.setRowFactory(new JourneyListCallback());
        }
    }


    @FXML
    public void showCart(Event event) {
        if (MainConfig.getUser().isAdmin()) {
            ViewLoader viewLoader = new ViewLoader((Node) event.getSource());
            viewLoader.loadScene("../view/addJourney.fxml", "Journeys");
        } else {
            ViewLoader viewLoader = new ViewLoader((Node) event.getSource());
            viewLoader.loadScene("../view/userJourneysList.fxml", "Journeys");
        }
    }

    @FXML
    void logOut(Event event) {
        MainConfig.reset();
        ViewLoader viewLoader = new ViewLoader((Node) event.getSource());
        viewLoader.loadScene("../view/loginView.fxml", "Journeys");
    }
}
