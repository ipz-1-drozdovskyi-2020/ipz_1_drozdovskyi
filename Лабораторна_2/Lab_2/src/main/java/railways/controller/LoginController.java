package railways.controller;

import javafx.event.ActionEvent;
import javafx.event.Event;
import javafx.fxml.FXML;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import railways.config.MainConfig;
import railways.network.client.Client;
import railways.network.model.User;
import railways.network.OperationType;
import railways.network.RequestCode;
import railways.network.Response;
import railways.controller_elements.Util;
import railways.controller_elements.ViewLoader;

public class LoginController {

    @FXML
    private Button loginButton;

    @FXML
    private PasswordField passwordField;

    @FXML
    private TextField usernameField;

    @FXML
    private Button registerButton;

    @FXML
    private void handleButtonAction(ActionEvent event) {
        String username = usernameField.getText();
        String password = passwordField.getText();

        if (username.isEmpty() || password.isEmpty()) {
            Util.showAlert("Login error", "Some field is empty");
            return;
        }

        Response response = Client.getInstance().send(
                OperationType.LOGIN,
                usernameField.getText(),
                passwordField.getText()
        );

        if (response.getRequestCode().equals(RequestCode.OK)) {
            MainConfig.setUser((User) response.getData());
            ViewLoader viewLoader = new ViewLoader((Node) event.getSource());
            viewLoader.loadScene("../view/journeysList.fxml", "Journeys");
        } else if (response.getRequestCode().equals(RequestCode.ERROR)) {
            Util.showAlert("Login error", response.getData().toString());
        }
    }

    @FXML
    void handleRegisterButton(Event event) {
        ViewLoader viewLoader = new ViewLoader((Node) event.getSource());
        viewLoader.loadScene("../view/registration.fxml", "Journeys");
    }
}
