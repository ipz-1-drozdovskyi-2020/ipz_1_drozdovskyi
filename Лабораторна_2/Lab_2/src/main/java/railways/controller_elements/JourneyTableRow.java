package railways.controller_elements;

import javafx.beans.property.SimpleStringProperty;
import railways.network.model.Journey;

import java.text.SimpleDateFormat;

public class JourneyTableRow {
    private Journey journey;
    private final SimpleStringProperty from;
    private final SimpleStringProperty to;
    private final SimpleStringProperty places;
    private final SimpleStringProperty date;

    public JourneyTableRow(
            Journey journey
    ) {
        this.journey = journey;
        this.from = new SimpleStringProperty(journey.getFrom());
        this.to = new SimpleStringProperty(journey.getTo());
        this.places = new SimpleStringProperty(journey.getPlaces().toString());
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy HH:mm");

        this.date = new SimpleStringProperty(
                simpleDateFormat.format(journey.getDate()));
    }

    public Journey toJourney() {
        return journey;
    }

    public String getFrom() {
        return from.get();
    }

    public String getTo() {
        return to.get();
    }

    public String getPlaces() {
        return places.get();
    }

    public String getDate() {
        return date.get();
    }

    public void setFrom(String from) {
        this.from.set(from);
    }

    public void setTo(String to) {
        this.to.set(to);
    }

    public void setPlaces(String places) {
        this.places.set(places);
    }

    public void setDate(String date) {
        this.date.set(date);
    }

}
