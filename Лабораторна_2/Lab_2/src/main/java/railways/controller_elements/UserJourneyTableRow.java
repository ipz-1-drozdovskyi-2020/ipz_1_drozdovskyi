package railways.controller_elements;

import javafx.beans.property.SimpleStringProperty;
import railways.network.model.UserJourney;

import java.text.SimpleDateFormat;

public class UserJourneyTableRow {
    private UserJourney userJourney;
    private final SimpleStringProperty from;
    private final SimpleStringProperty to;
    private final SimpleStringProperty place;
    private final SimpleStringProperty date;

    public UserJourneyTableRow(
            UserJourney userJourney
    ) {
        this.userJourney = userJourney;
        this.from = new SimpleStringProperty(userJourney.getJourney().getFrom());
        this.to = new SimpleStringProperty(userJourney.getJourney().getTo());
        this.place = new SimpleStringProperty(Long.toString(userJourney.getPlace()));
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy HH:mm");

        this.date = new SimpleStringProperty(
                simpleDateFormat.format(userJourney.getJourney().getDate()));
    }

    public UserJourney toUserJourney() {
        return userJourney;
    }

    public String getFrom() {
        return from.get();
    }

    public String getTo() {
        return to.get();
    }

    public String getPlace() {
        return place.get();
    }

    public String getDate() {
        return date.get();
    }

    public void setFrom(String from) {
        this.from.set(from);
    }

    public void setTo(String to) {
        this.to.set(to);
    }

    public void setPlace(String place) {
        this.place.set(place);
    }

    public void setDate(String date) {
        this.date.set(date);
    }
}
