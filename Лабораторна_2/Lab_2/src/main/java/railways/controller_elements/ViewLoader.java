package railways.controller_elements;

import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import railways.config.ApplicationProperties;

import java.io.IOException;

public class ViewLoader {
    private Node source;

    public ViewLoader(Node source) {
        this.source = source;
    }

    public Boolean loadScene(String path, String title) {
        FXMLLoader loader = new FXMLLoader(getClass().getResource(path));
        Parent root;

        try {
                root = loader.load();

        } catch (IOException exception) {
            exception.printStackTrace();
            Util.showAlert("Cant Load View","Error");
            return false;
        }

        Stage stage = new Stage();
        stage.setTitle(title);
        stage.setScene(new Scene(root, ApplicationProperties.getWidth(), ApplicationProperties.getHeight()));
        stage.show();
        if(source.getScene()!=null) {
            source.getScene().getWindow().hide();
        }
        return true;
    }
}
