package railways;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import railways.config.ApplicationProperties;


public class Main extends Application {

    @Override
    public void start(Stage stage) throws Exception {
        Parent root = FXMLLoader.load(getClass().getResource("view/loginView.fxml"));
        Scene scene = new Scene(root, ApplicationProperties.getWidth(), ApplicationProperties.getHeight());
        stage.setTitle("Journeys");
        stage.setScene(scene);
        stage.show();
    }


    public static void main(String[] args) {
        launch(args);
    }
}
