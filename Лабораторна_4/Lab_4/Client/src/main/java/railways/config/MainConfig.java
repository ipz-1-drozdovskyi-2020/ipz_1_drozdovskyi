package railways.config;

import railways.network.model.Journey;
import railways.network.model.User;

public class MainConfig {
    private static User user;
    private static Journey journey;

    public static void reset() {
        user = null;
        journey = null;
    }

    public static User getUser() {
        return user;
    }

    public static void setUser(User user) {
        MainConfig.user = user;
    }

    public static Journey getJourney() {
        return journey;
    }

    public static void setJourney(Journey journey) {
        MainConfig.journey = journey;
    }
}
