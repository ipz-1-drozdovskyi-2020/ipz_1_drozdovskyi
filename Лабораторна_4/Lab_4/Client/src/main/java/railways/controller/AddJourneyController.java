package railways.controller;

import javafx.event.Event;
import javafx.fxml.FXML;
import javafx.scene.Node;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TextField;
import railways.network.client.Client;
import railways.network.model.Journey;
import railways.config.ApplicationProperties;
import railways.network.OperationType;
import railways.controller_elements.Util;
import railways.controller_elements.ViewLoader;

import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Date;
import java.util.regex.Pattern;

public class AddJourneyController {

    @FXML
    private TextField from;

    @FXML
    private TextField to;

    @FXML
    private TextField places;

    @FXML
    private DatePicker date;

    @FXML
    private TextField hour;

    @FXML
    private TextField price;

    @FXML
    void addJourney() {
        LocalDate date = this.date.getValue();
        String from = this.from.getText();
        String to = this.to.getText();
        String places = this.places.getText();
        String hour = this.hour.getText();
        String price = this.price.getText();

        if (from.isEmpty() || to.isEmpty() || places.isEmpty() || date == null) {
            Util.showAlert("Error", "Some fields is empty");
            return;
        }
        if (!Pattern.matches("\\d+", places)) {
            Util.showAlert("Error", "Places must be number");
            return;
        }
        if (!Pattern.matches("\\d+", price)) {
            Util.showAlert("Error", "Price must be number");
            return;
        }
        if (!Util.allMatchPattern("[a-zA-Z]+", from, to)) {
            Util.showAlert("Error", "From and To must contains only letters");
            return;
        }
        if (!Pattern.matches("\\d+", hour)) {
            Util.showAlert("Error", "Hour must be number");
            return;
        }
        long placesValue = Long.valueOf(places);

        if (placesValue > ApplicationProperties.getMaxPlaces()) {
            Util.showAlert("Error", "Max places is " + ApplicationProperties.getMaxPlaces());
            return;
        }

        int hourValue = Integer.valueOf(hour);
        if (0 > hourValue || hourValue >= 24) {
            Util.showAlert("Error", "Hour must be in range 0 to 23");
            return;
        }

        Date journeyDate = Date.from(date.atStartOfDay(ZoneId.systemDefault()).toInstant());
        journeyDate.setHours(hourValue);

        if ((new Date()).compareTo(journeyDate) >= 0) {
            Util.showAlert("Error", "Can't set date less that now");
            return;
        }

        Journey journey = new Journey(null, from, to, placesValue, Long.valueOf(price), journeyDate);

        Client.getInstance().send(OperationType.ADD_JOURNEY, journey);

        ViewLoader viewLoader = new ViewLoader(this.from);
        viewLoader.loadScene("../view/journeysList.fxml", "Journeys");
    }

    @FXML
    void cancelAdding(Event event) {
        ViewLoader viewLoader = new ViewLoader((Node) event.getSource());
        viewLoader.loadScene("../view/journeysList.fxml", "Journeys");
    }

}
