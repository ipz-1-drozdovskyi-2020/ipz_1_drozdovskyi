package railways.controller;

import javafx.event.Event;
import javafx.fxml.FXML;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import railways.config.MainConfig;
import railways.network.client.Client;
import railways.network.model.Journey;
import railways.network.model.UserJourney;
import railways.network.OperationType;
import railways.network.RequestCode;
import railways.network.Response;
import railways.controller_elements.Util;
import railways.controller_elements.ViewLoader;

import java.util.List;

public class GetWayController {
    @FXML
    private VBox trainScheme;

    @FXML
    private FlowPane firstRow;

    @FXML
    private FlowPane secondRow;

    @FXML
    private VBox journeyInfo;

    public void initData(Journey journey) {
        Label places = new Label("Places " + journey.getPlaces().toString());
        Label date = new Label("Date " + journey.getDate().toString());
        Label price = new Label("Price " + journey.getPrice());
        Label from = new Label("From " + journey.getFrom());
        Label to = new Label("To " + journey.getTo());

        FontWeight fontWeight = FontWeight.NORMAL;
        String font = "Tahoma";
        int mainTextWeight = 20;
        int textWeight = 15;

        from.setFont(Font.font(font, fontWeight, mainTextWeight));
        to.setFont(Font.font(font, fontWeight, mainTextWeight));
        places.setFont(Font.font(font, fontWeight, textWeight));
        price.setFont(Font.font(font, fontWeight, textWeight));
        date.setFont(Font.font(font, fontWeight, textWeight));

        journeyInfo.getChildren().addAll(from, to, places, date, price);

        long placesCount = journey.getPlaces();
        Response response = Client.getInstance().send(
                OperationType.GET_RECEIVED_PLACES,
                MainConfig.getJourney()
        );

        List<Long> integers = (List<Long>) response.getData();

        for (long i = 1; i < placesCount + 1; i++) {
            Button button = new Button(Long.toString(i));
            long finalI = i;

            button.setOnAction(event -> {
                if (Util.confirmDialog(
                        "Are you sure?",
                        "Buy ticket",
                        "You want to buy ticket?"
                )) {
                    Response receiveJourneyResponse = Client.getInstance().send(
                            OperationType.RECEIVE_JOURNEY,
                            new UserJourney(
                                    finalI,
                                    MainConfig.getUser(),
                                    MainConfig.getJourney()
                            )
                    );
                    if (receiveJourneyResponse.getRequestCode().equals(RequestCode.ERROR)) {
                        Util.showAlert("Error", "Someone already receive this place");
                    } else {
                        button.setDisable(true);
                    }
                }
            });

            button.setMinSize(50, 50);
            button.setTextFill(Color.BLUE);
            button.setAlignment(Pos.CENTER);

            if (integers.contains(i)) {
                button.setDisable(true);
            }

            if (i % 2 == 0) {
                firstRow.getChildren().add(button);
            } else {
                secondRow.getChildren().add(button);
            }
        }

    }

    @FXML
    public void initialize() {
        trainScheme.setAlignment(Pos.CENTER);

        initData(MainConfig.getJourney());
    }

    @FXML
    public void goBackToJourneysList(Event event) {
        ViewLoader viewLoader = new ViewLoader((Node) event.getSource());
        viewLoader.loadScene("../view/journeysList.fxml", "Journeys");
    }
}
