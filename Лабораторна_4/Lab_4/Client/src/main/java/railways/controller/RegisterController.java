package railways.controller;

import javafx.event.Event;
import javafx.fxml.FXML;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import railways.config.MainConfig;
import railways.network.client.Client;
import railways.network.model.User;
import railways.network.OperationType;
import railways.network.RequestCode;
import railways.network.Response;
import railways.controller_elements.Util;
import railways.controller_elements.ViewLoader;

public class RegisterController {
    @FXML
    private Button registrationButton;

    @FXML
    private Button cancelButton;

    @FXML
    private PasswordField confirmPassword;

    @FXML
    private PasswordField password;

    @FXML
    private TextField name;

    @FXML
    private TextField surname;

    @FXML
    private TextField username;

    @FXML
    private TextField email;

    @FXML
    public void registerUser() {
        String confirmPassword = this.confirmPassword.getText();
        String password = this.password.getText();
        String name = this.name.getText();
        String surname = this.surname.getText();
        String username = this.username.getText();
        String email = this.email.getText();

        if (confirmPassword.isEmpty() || password.isEmpty() ||
                name.isEmpty() || surname.isEmpty()
                || username.isEmpty() || email.isEmpty()) {
            Util.showAlert("Incorrect inputs",
                    "Some fields is empty",
                    "Fields can't be empty");
            return;
        }
        if (username.length() < 6) {
            Util.showAlert("Incorrect inputs",
                    "Username must be more than 6 chars");
            return;
        }
        if (password.length() < 6) {
            Util.showAlert("Incorrect inputs",
                    "Password must be more than 6 chars");
            return;
        }
        if (email.length() < 6) {
            Util.showAlert("Incorrect inputs",
                    "Email must be more than 6 chars");
            return;
        }
        if (!password.equals(confirmPassword)) {
            Util.showAlert("Incorrect inputs",
                    "Password and confirm password isn't equals");
            return;
        }
        if (!Util.allMatchPattern("[a-zA-Z]{3,20}", name, surname)) {
            Util.showAlert("Incorrect inputs",
                    "Name and Surname must contains only letters " +
                            "and length must be between 3 and 20");
            return;
        }
        if (!Util.allMatchPattern("[a-zA-Z0-9_-]{6,20}", username, password)) {
            Util.showAlert("Incorrect inputs",
                    "Incorrect username or password must contains only" +
                            " letters and numbers and length must be between 6 and 20");
            return;
        }

        User user = new User(null, username, password, name, surname, email);

        Response response = Client.getInstance().send(OperationType.REGISTER_USER, user);

        if (response.getRequestCode().equals(RequestCode.ERROR)) {
            Util.showAlert("Error",
                    response.getData().toString(),
                    "");
        } else if (response.getRequestCode().equals(RequestCode.OK)) {
            user = (User) response.getData();
            MainConfig.setUser(user);
            ViewLoader viewLoader = new ViewLoader(this.cancelButton);
            viewLoader.loadScene("../view/journeysList.fxml", "Journeys");
        }
    }


    @FXML
    public void cancelRegistration(Event event) {
        ViewLoader viewLoader = new ViewLoader((Node) event.getSource());
        viewLoader.loadScene("../view/loginView.fxml", "Journeys");
    }

}
