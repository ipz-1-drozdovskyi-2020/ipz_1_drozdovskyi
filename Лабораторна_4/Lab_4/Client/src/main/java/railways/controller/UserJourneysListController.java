package railways.controller;

import javafx.collections.FXCollections;
import javafx.event.Event;
import javafx.fxml.FXML;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import railways.config.MainConfig;
import railways.network.client.Client;
import railways.network.model.UserJourney;
import railways.network.OperationType;
import railways.network.RequestCode;
import railways.network.Response;
import railways.controller_elements.UserJourneyListCallback;
import railways.controller_elements.UserJourneyTableRow;
import railways.controller_elements.ViewLoader;

import java.util.List;
import java.util.stream.Collectors;

public class UserJourneysListController {
    @FXML
    private TableView table;

    @FXML
    private Button userJourneys;

    private TableColumn from = new TableColumn("From");
    private TableColumn to = new TableColumn("To");
    private TableColumn places = new TableColumn("Place");
    private TableColumn date = new TableColumn("Date");


    @FXML
    void initialize() {
        Response response = Client.getInstance().send(
                OperationType.GET_USER_JOURNEYS,
                MainConfig.getUser()
        );

        if (response.getRequestCode().equals(RequestCode.CONNECTION_ERROR)) {
            return;
        }

        from.setCellValueFactory(new PropertyValueFactory<UserJourneyTableRow, String>("from"));
        to.setCellValueFactory(new PropertyValueFactory<UserJourneyTableRow, String>("to"));
        places.setCellValueFactory(new PropertyValueFactory<UserJourneyTableRow, String>("place"));
        date.setCellValueFactory(new PropertyValueFactory<UserJourneyTableRow, String>("date"));

        table.setItems(FXCollections.observableList(((List<UserJourney>) response.getData())
                .stream().map(UserJourneyTableRow::new).collect(Collectors.toList())
        ));

        table.setEditable(false);
        table.setColumnResizePolicy(TableView.CONSTRAINED_RESIZE_POLICY);
        table.getColumns().addAll(from, to, places, date);
        table.setRowFactory(new UserJourneyListCallback(table));
    }

    @FXML
    public void goBackToJourneysList(Event event) {
        ViewLoader viewLoader = new ViewLoader((Node) event.getSource());
        viewLoader.loadScene("../view/journeysList.fxml", "Journeys");
    }
}
