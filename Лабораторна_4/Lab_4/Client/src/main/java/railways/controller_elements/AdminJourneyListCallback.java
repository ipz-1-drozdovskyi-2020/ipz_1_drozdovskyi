package railways.controller_elements;

import javafx.scene.control.TableRow;
import javafx.scene.control.TableView;
import javafx.util.Callback;
import railways.network.client.Client;
import railways.network.OperationType;

public class AdminJourneyListCallback implements Callback<TableView, TableRow> {

    private TableView<JourneyTableRow> tableView;

    public AdminJourneyListCallback(TableView<JourneyTableRow> tableView) {
        this.tableView = tableView;
    }

    @Override
    public TableRow call(TableView param) {
        TableRow<JourneyTableRow> tableRow = new TableRow<>();

        tableRow.setOnMouseClicked(
                event -> {
                    if (!tableRow.isEmpty()) {

                        if (Util.confirmDialog(
                                "Are you sure",
                                "You want delete journey",
                                "You want delete journey"
                        )) {
                            Client.getInstance().send(
                                    OperationType.DELETE_JOURNEY, tableRow.getItem().toJourney()
                            );
                            JourneyTableRow journeyTableRow = tableView
                                    .getSelectionModel()
                                    .getSelectedItem();

                            tableView.getItems().remove(journeyTableRow);
                        }
                    }
                }
        );

        return tableRow;
    }
}
