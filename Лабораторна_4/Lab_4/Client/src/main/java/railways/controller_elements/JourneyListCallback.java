package railways.controller_elements;

import javafx.scene.Node;
import javafx.scene.control.TableRow;
import javafx.scene.control.TableView;
import javafx.util.Callback;
import railways.config.MainConfig;

public class JourneyListCallback implements Callback<TableView, TableRow> {

    @Override
    public TableRow call(TableView param) {
        TableRow<JourneyTableRow> tableRow = new TableRow<>();

        tableRow.setOnMouseClicked(
                event -> {
                    if (!tableRow.isEmpty()) {
                        MainConfig.setJourney(tableRow.getItem().toJourney());
                        ViewLoader viewLoader = new ViewLoader((Node) event.getSource());
                        viewLoader.loadScene("../view/getWay.fxml", "Journeys");
                    }
                }
        );

        return tableRow;
    }
}
