package railways.controller_elements;

import javafx.scene.control.TableRow;
import javafx.scene.control.TableView;
import javafx.util.Callback;
import railways.network.client.Client;
import railways.network.model.UserJourney;
import railways.network.OperationType;

public class UserJourneyListCallback implements Callback<TableView, TableRow> {

    private TableView<UserJourneyTableRow> tableView;

    public UserJourneyListCallback(TableView<UserJourneyTableRow> tableView) {
        this.tableView = tableView;
    }

    @Override
    public TableRow call(TableView param) {
        TableRow<UserJourneyTableRow> tableRow = new TableRow<>();
        tableRow.setOnMouseClicked(
                event -> {
                    if (!tableRow.isEmpty()) {

                        if (Util.confirmDialog("Are you sure?",
                                "Delete ticket?",
                                "Are you really want delete ticket?"
                        )) {
                            UserJourney userJourney = tableRow.getItem().toUserJourney();

                            Client.getInstance()
                                    .send(OperationType.DELETE_USER_JOURNEY, userJourney);

                            UserJourneyTableRow userJourneyTableRow = tableView
                                    .getSelectionModel()
                                    .getSelectedItem();

                            tableView.getItems().remove(userJourneyTableRow);
                        }
                    }
                }
        );

        return tableRow;
    }
}
