
import javafx.fxml.FXMLLoader;

import org.junit.Assert;
import org.junit.Test;

import railways.config.ApplicationProperties;
import railways.controller_elements.Util;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import java.util.regex.Pattern;


import static org.junit.Assert.fail;


public class ClientTest {

    @Test
    public void ApplicationPropertiesFileLoadTest()  {
        Properties properties = new Properties();
        try {
            properties.load(new FileInputStream("resources/config.properties"));
        }catch (FileNotFoundException e){
            fail("Properties file not found");
        }catch (IOException e){
            fail(e.toString());
        }
    }
    @Test
    public void ApplicationPropertiesClassTest(){
        Properties properties = new Properties();
        try {
            properties.load(new FileInputStream("resources/config.properties"));

        }catch (Exception e){
            fail(e.toString());
        }

        Assert.assertEquals(ApplicationProperties.getHeight(),Integer.parseInt(properties.getProperty("height")));
        Assert.assertEquals(ApplicationProperties.getHost(),properties.getProperty("host"));
        Assert.assertEquals(ApplicationProperties.getPort(),Integer.parseInt(properties.getProperty("port")));
        Assert.assertEquals(ApplicationProperties.getWidth(),Integer.parseInt(properties.getProperty("width")));
        Assert.assertEquals(ApplicationProperties.getMaxPlaces(),40);
    }
    @Test
    public void JourneysListLoaderTest(){
        FXMLLoader loader = new FXMLLoader(getClass().getResource("../view/journeysList.fxml"));
        Assert.assertNotNull(loader);
    }
    @Test
    public void AddJourneyLoaderTest(){
        FXMLLoader loader = new FXMLLoader(getClass().getResource("../view/addJourney.fxml"));
        Assert.assertNotNull(loader);
    }
    @Test
    public void GetWayLoaderTest(){
        FXMLLoader loader = new FXMLLoader(getClass().getResource("../view/getWay.fxml"));
        Assert.assertNotNull(loader);
    }
    @Test
    public void userJourneysListLoaderTest(){
        FXMLLoader loader = new FXMLLoader(getClass().getResource("../view/userJourneysList.fxml"));
        Assert.assertNotNull(loader);
    }
    @Test
    public void registrationLoaderTest(){
        FXMLLoader loader = new FXMLLoader(getClass().getResource("../view/registration.fxml"));
        Assert.assertNotNull(loader);
    }
    @Test
    public void LoginLoaderTest(){
        FXMLLoader loader = new FXMLLoader(getClass().getResource("../view/loginView.fxml"));
        Assert.assertNotNull(loader);
    }
    @Test
    public void MatchPatternTest(){
        String a = "asmsm@gmail.com";
        String b = "maxym@yahoo.com";

        Assert.assertTrue(Util.allMatchPattern("^([a-zA-Z0-9_\\-\\.]+)@([a-zA-Z0-9_\\-\\.]+)\\.([a-zA-Z]{2,5})$", a,b));
    }
//    @Test
//    public void (){
//        try {
//            Util.showConnectionErrorNotice();
//        }catch (Exception e){
//            fail("Cant show window");
//        }
//    }

}
