package railways.test;

import org.junit.Assert;
import org.junit.Test;

import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import railways.network.OperationType;
import railways.server.request_handlers.handlers_impl.*;
import railways.server.request_handlers.manager_impl.RequestManagerImpl;
@SpringBootTest
@RunWith(SpringRunner.class)

public class ServerTests  {
    @Autowired
    RequestManagerImpl requestManager ;

    @Test
  public void  RequestManagerLoginTest() {
    Assert.assertTrue(requestManager.handleRequest(OperationType.LOGIN) instanceof LoginRequestHandler);
    }

    @Test
    public void  RequestManagerRegisterTest() {
        Assert.assertTrue(requestManager.handleRequest(OperationType.REGISTER_USER) instanceof RegisterUserRequestHandler);
    }

    @Test
    public void  RequestManagerReceiveJourneyTest() {
        Assert.assertTrue(requestManager.handleRequest(OperationType.RECEIVE_JOURNEY) instanceof ReceiveJourneyRequestHandler);
    }
    @Test
    public void  RequestManagerGetJourneysTest() {
        Assert.assertTrue(requestManager.handleRequest(OperationType.GET_JOURNEYS) instanceof GetAllJourneysRequestHandler);
    }
    @Test
    public void  RequestManagerAddJourneyTest() {
        Assert.assertTrue(requestManager.handleRequest(OperationType.ADD_JOURNEY) instanceof AddJourneyRequestHandler);
    }
    @Test
    public void  RequestManagerDeleteUserTest() {
        Assert.assertTrue(requestManager.handleRequest(OperationType.DELETE) instanceof DeleteUserRequestHandler);
    }
    @Test
    public void  RequestManagerDeleteUserJourneyTest() {
        Assert.assertTrue(requestManager.handleRequest(OperationType.DELETE_USER_JOURNEY) instanceof DeleteReceivedJourneyRequestHandler);
    }
    @Test
    public void  RequestManagerGetReceivedPlacesTest() {
        Assert.assertTrue(requestManager.handleRequest(OperationType.GET_RECEIVED_PLACES) instanceof GetReceivedPlacesRequestHandler);
    }
    @Test
    public void  RequestManagerGetJourneysCountTest() {
        Assert.assertTrue(requestManager.handleRequest(OperationType.GET_JOURNEYS_COUNT) instanceof JourneysCountRequestHandler);
    }
    @Test
    public void  RequestManagerGetUserJourneysTest() {
        Assert.assertTrue(requestManager.handleRequest(OperationType.GET_USER_JOURNEYS) instanceof GetUserJourneysRequestHandler);
    }

}
